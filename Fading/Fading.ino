int pin = 13;
int t = 10;

void setup() {
  pinMode(pin, OUTPUT);
}

void loop() {
  
  for (int v = 0; v < 256; v++) {
    analogWrite(pin, v);
    delay(t);
  }
  
  for (int v = 255; v >= 0; v--) {
    analogWrite(pin, v);
    delay(t);
  }

}
