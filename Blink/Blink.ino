/* 
 *  Turns an LED on for one second, then off for one second repeatedly
 *  
 *  This example code is in public domain
 */
// setup code runs just once at the beginning
void setup() {
  // Initialize the digital pin 13 as an output
  // Pin 13 has an LED connected on most Arduino boards
  pinMode(13, OUTPUT);
}

// loop code will run repeatedly amd indefinitely
void loop() {
  digitalWrite(13, HIGH); // Set the LED on
  delay(1000);             // Wait for a second
  digitalWrite(13, LOW);  // Set the LED off
  delay(1000);            // Wait for a second
}
