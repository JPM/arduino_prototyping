#include <MeetAndroid.h>

MeetAndroid meetAndroid;
int led = 13;
int t = 20;
int fadLen = 7;
char fading[7] = "fading";

void setup()  
{
  Serial.begin(9600); 
  
  meetAndroid.registerFunction(setLedEvent, 'A');
  meetAndroid.registerFunction(setFadingEvent, 'B');
  
  pinMode(led, OUTPUT);
}

void loop()
{
  meetAndroid.receive();
}

void setLedEvent(byte flag, byte numOfValues)
{
  int v = meetAndroid.getInt();
  analogWrite(led, v);
}

void setFadingEvent(byte flag, byte numOfValues)
{
  if(numOfValues == 0)
    return;
  
  int len = meetAndroid.bufferLength();
  char in[len];
  meetAndroid.getString(in);
  Serial.println(len);
  Serial.println(in);

  if(len != fadLen)
    return;
  
  for(int i = 0; i < len; i++)
    if(in[i] != fading[i])
      return;
  
  for (int v = 0; v < 256; v++) {
    analogWrite(led, v);
    delay(t);
  }
}
