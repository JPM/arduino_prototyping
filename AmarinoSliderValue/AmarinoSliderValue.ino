#include <MeetAndroid.h>

int out = 13;
int t = 100;
MeetAndroid android;

void setup() {
  Serial.begin(9600);
  pinMode(out, OUTPUT);
  android.registerFunction(setLed, 'A');
}

void loop() {
  android.receive();
}

void setLed(byte flag, byte numOfValues) {
  if(flag != 'A' || numOfValues == 0)
    return;

  int value = 2.55 * android.getInt();
  analogWrite(out, value);
  android.send(value);
}

