int in = A0;
int out = 9;
int t = 20;

void setup() {
  pinMode(out, OUTPUT);
  pinMode(in, INPUT);
}

void loop() {
  int value = analogRead(in); // 0 < value < 1024

  // Converts the range 0-1024 to 0-256
  analogWrite(out, value/4); // // 0 < value < 256

  delay(t);
}
