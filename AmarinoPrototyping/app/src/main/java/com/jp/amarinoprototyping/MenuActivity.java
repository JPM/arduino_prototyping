package com.jp.amarinoprototyping;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onSendEventClick(View v){
        startActivity(new Intent(this, MainActivity.class));
    }

    public void onReceiveEventClick(View v){
        startActivity(new Intent(this, ReceiveEventActivity.class));
    }

    public void onSliderClick(View v){
        startActivity(new Intent(this, SliderActivity.class));
    }

    public void onRGBClick(View v){
        startActivity(new Intent(this, RGBActivity.class));
    }
}
