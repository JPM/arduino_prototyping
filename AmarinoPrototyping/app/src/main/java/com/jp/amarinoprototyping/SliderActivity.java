package com.jp.amarinoprototyping;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import at.abraxas.amarino.Amarino;
import at.abraxas.amarino.AmarinoIntent;

public class SliderActivity extends AppCompatActivity {
    public static final String TAG = "AmarinoSldr";
    private BroadcastReceiver mAmarinoReceiver = null;
    private BroadcastReceiver mConnectionReceiver = null;
    private static final String ADDRESS = MainActivity.ADDRESS;

    private TextView mValue;
    private SeekBar mSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        mValue = (TextView) findViewById(R.id.textValue);
        mSlider = (SeekBar) findViewById(R.id.slider);
        mSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d(TAG, "Sending value: " + i);
                Amarino.sendDataToArduino(SliderActivity.this, ADDRESS, 'A', i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_connect) {
            registerConnectionReceiver();
            Amarino.connect(this, ADDRESS);
            return true;
        } else if (id == R.id.action_disconnect) {
            Amarino.disconnect(this, ADDRESS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void registerConnectionReceiver() {
        if (mConnectionReceiver != null) {
            return;
        }
        IntentFilter actionsFilter = new IntentFilter();
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_DISCONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTION_FAILED);
        actionsFilter.addAction(AmarinoIntent.ACTION_PAIRING_REQUESTED);

        mConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "Connection Receiver::Action: " + action);
                if(AmarinoIntent.ACTION_CONNECTED.equals(action)) {
                    Toast.makeText(SliderActivity.this, "Amarino Connected", Toast.LENGTH_SHORT).show();
                    registerDataReceiver();
                } else {
                    Toast.makeText(SliderActivity.this, "Amarino Disconnected", Toast.LENGTH_SHORT).show();
                    unregister(mAmarinoReceiver);
                }
            }
        };

        getApplicationContext().registerReceiver(mConnectionReceiver, actionsFilter);
    }

    private void registerDataReceiver(){
        if(mAmarinoReceiver!=null)
            return;

        IntentFilter filter = new IntentFilter();
        filter.addAction(AmarinoIntent.ACTION_RECEIVED);

        mAmarinoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(AmarinoIntent.ACTION_RECEIVED.equals(action)){
                    int type = intent.getIntExtra(AmarinoIntent.EXTRA_DATA_TYPE, -1);
                    Log.d(TAG, "Amarino Received::Action_Received::type" + type);
                    if(AmarinoIntent.STRING_EXTRA == type) {
                        String data = intent.getStringExtra(AmarinoIntent.EXTRA_DATA);
                        Log.d(TAG, data);
                        mValue.setText(data);
                    }
                }
            }
        };

        this.registerReceiver(mAmarinoReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        unregister(mAmarinoReceiver);
        unregister(mConnectionReceiver);
        super.onDestroy();
    }

    private void unregister(BroadcastReceiver receiver) {
        if(receiver != null) {
            try {
                unregisterReceiver(receiver);
                receiver = null;
            } catch (Exception e) {
                // Do nothing
            }
        }

    }


}
