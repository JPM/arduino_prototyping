package com.jp.amarinoprototyping;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import at.abraxas.amarino.Amarino;
import at.abraxas.amarino.AmarinoIntent;

public class AmarinoBaseActivity extends AppCompatActivity {

    public static final String TAG = "Amarino";
    private static final String ADDRESS = "00:11:09:01:06:22";

    private BroadcastReceiver mAmarinoReceiver = null;
    private BroadcastReceiver mConnectionReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amarino_base);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_connect) {
            registerConnectionReceiver();
            Amarino.connect(this, ADDRESS);
            return true;
        } else if (id == R.id.action_disconnect) {
            Amarino.disconnect(this, ADDRESS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void registerConnectionReceiver() {
        if (mConnectionReceiver != null) {
            return;
        }
        IntentFilter actionsFilter = new IntentFilter();
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_DISCONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTION_FAILED);
        actionsFilter.addAction(AmarinoIntent.ACTION_PAIRING_REQUESTED);

        mConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "Connection Receiver::Action: " + action);
                if(AmarinoIntent.ACTION_CONNECTED.equals(action)) {
                    Toast.makeText(AmarinoBaseActivity.this, "Amarino Connected", Toast.LENGTH_SHORT).show();
                    registerDataReceiver();
                } else {
                    Toast.makeText(AmarinoBaseActivity.this, "Amarino Disconnected", Toast.LENGTH_SHORT).show();
                    unregister(mAmarinoReceiver);
                }
            }
        };

        getApplicationContext().registerReceiver(mConnectionReceiver, actionsFilter);
    }

    private void registerDataReceiver(){
        if(mAmarinoReceiver!=null)
            return;

        IntentFilter filter = new IntentFilter();
        filter.addAction(AmarinoIntent.ACTION_RECEIVED);

        mAmarinoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(AmarinoIntent.ACTION_RECEIVED.equals(action)){
                    int type = intent.getIntExtra(AmarinoIntent.EXTRA_DATA_TYPE, -1);

                    if(AmarinoIntent.STRING_EXTRA == type) {
                        String data = intent.getStringExtra(AmarinoIntent.EXTRA_DATA);
                        Log.d(TAG, "Amarino Received: " + data);
                    } else {
                        Log.d(TAG, "Amarino Received no String: " + intent.getDataString());
                    }
                }
            }
        };

        this.registerReceiver(mAmarinoReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        unregister(mAmarinoReceiver);
        unregister(mConnectionReceiver);
        super.onDestroy();
    }

    private void unregister(BroadcastReceiver receiver) {
        if(receiver != null) {
            try {
                unregisterReceiver(receiver);
                receiver = null;
            } catch (Exception e) {
                // Do nothing
            }
        }

    }


}
