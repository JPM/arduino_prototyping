package com.jp.amarinoprototyping;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import at.abraxas.amarino.Amarino;
import at.abraxas.amarino.AmarinoIntent;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "Amarino";
    private BroadcastReceiver mAmarinoReceiver = null;
    private BroadcastReceiver mConnectionReceiver = null;
    public static final String ADDRESS = "00:11:09:01:06:22";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_connect) {
            registerConnectionReceiver();
            Amarino.connect(this, ADDRESS);
            return true;
        } else if (id == R.id.action_disconnect) {
            Amarino.disconnect(this, ADDRESS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void registerConnectionReceiver() {
        if (mConnectionReceiver != null) {
            return;
        }
        IntentFilter actionsFilter = new IntentFilter();
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_DISCONNECTED);
        actionsFilter.addAction(AmarinoIntent.ACTION_CONNECTION_FAILED);
        actionsFilter.addAction(AmarinoIntent.ACTION_PAIRING_REQUESTED);

        mConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "Connection Receiver::Action: " + action);
                if(AmarinoIntent.ACTION_CONNECTED.equals(action)) {
                    Toast.makeText(MainActivity.this, "Amarino Connected", Toast.LENGTH_SHORT).show();
                    registerDataReceiver();
                } else {
                    Toast.makeText(MainActivity.this, "Amarino Disconnected", Toast.LENGTH_SHORT).show();
                    if(mAmarinoReceiver != null)
                        unregisterReceiver(mAmarinoReceiver);
                }
            }
        };

        getApplicationContext().registerReceiver(mConnectionReceiver, actionsFilter);
    }

    private void registerDataReceiver(){
        if(mAmarinoReceiver!=null)
            return;

        IntentFilter filter = new IntentFilter();
        filter.addAction(AmarinoIntent.ACTION_RECEIVED);

        mAmarinoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(AmarinoIntent.ACTION_RECEIVED.equals(action)){
                    int type = intent.getIntExtra(AmarinoIntent.EXTRA_DATA_TYPE, -1);
                    Log.d(TAG, "Amarino Received::Action_Received::type" + type);
                    if(AmarinoIntent.STRING_EXTRA == type) {
                        String data = intent.getStringExtra(AmarinoIntent.EXTRA_DATA);
                        Log.d(TAG, data);
                        Toast.makeText(MainActivity.this, data, Toast.LENGTH_LONG).show();
                    }
                }
            }
        };

        this.registerReceiver(mAmarinoReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        unregister(mAmarinoReceiver);
        unregister(mConnectionReceiver);
        super.onDestroy();
    }

    private void unregister(BroadcastReceiver receiver) {
        if(receiver != null) {
            try {
                unregisterReceiver(receiver);
            } catch (Exception e) {
                // Do nothing
            }
        }

    }

    public void onSendClick(View v) {
        String flag = ((EditText)findViewById(R.id.textFlag)).getText().toString();
        if(!TextUtils.isEmpty(flag)) {
            Log.d(TAG, "Flag: " + flag);
            Log.d(TAG, "chatAt: " + flag.charAt(0));
            Amarino.sendDataToArduino(this, ADDRESS, flag.charAt(0), ((EditText)findViewById(R.id.textSend)).getText().toString());
        } else
            Toast.makeText(this, "Flag must have at least one character", Toast.LENGTH_LONG);
    }

}
