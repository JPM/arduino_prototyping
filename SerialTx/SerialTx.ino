int i = 1;
int baudrate = 9600;
int t = 1000;

void setup() {
  Serial.begin(baudrate);
}

void loop() {
  Serial.print("Hello Arduino ");
  Serial.print(i++);
  Serial.println("!");
  delay(t);
}
