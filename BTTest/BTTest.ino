#include <MeetAndroid.h>

MeetAndroid meetAndroid;
int onboardLed = 13;

void setup()  
{
  Serial.begin(9600); 
  
  meetAndroid.registerFunction(testEvent, 'A');
  meetAndroid.registerFunction(testEvent, 'B');  
  meetAndroid.registerFunction(testEvent, 'C');  

  pinMode(onboardLed, OUTPUT);
  digitalWrite(onboardLed, HIGH);

}

void loop()
{
  meetAndroid.receive(); // you need to keep this in your loop() to receive events
}

void testEvent(byte flag, byte numOfValues)
{
  if(flag == 'A')
    meetAndroid.send("A received!");
  if(flag == 'B')
    meetAndroid.send("B received!");
  if(flag == 'C')
    meetAndroid.send("C received!");
    
  flushLed(500);
  flushLed(1000);
}

void flushLed(int time)
{
  digitalWrite(onboardLed, LOW);
  delay(time);
  digitalWrite(onboardLed, HIGH);
  delay(time);
}

