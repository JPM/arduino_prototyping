int in = A0;
int out = 13;
int threshold = 512;
int t = 20;

void setup() {
  pinMode(out, OUTPUT);
  pinMode(in, INPUT);
}

void loop() {
  int value = analogRead(in);
  
  if( value > threshold)
    digitalWrite(out, HIGH);
  else
    digitalWrite(out, LOW);

  delay(t);
}
