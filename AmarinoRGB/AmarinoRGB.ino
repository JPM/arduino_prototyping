#include <MeetAndroid.h>

MeetAndroid meetAndroid;

int blue = 13;
int green = 12;
int red = 11;

void setup()  
{
  Serial.begin(9600); 
  
  meetAndroid.registerFunction(setEvent, 'A');
  meetAndroid.registerFunction(setEvent, 'B');
  meetAndroid.registerFunction(setEvent, 'C');
  
  
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
}

void loop()
{
  meetAndroid.receive();
}

void setEvent(byte flag, byte numOfValues)
{
  int v = meetAndroid.getInt();

  if(flag == 'A')
    analogWrite(green, v);
  else if(flag == 'B')
    analogWrite(blue, v);
  else if(flag == 'C')
    analogWrite(red, v);
}

